# ansible

Saul Exposito public ansible repository

Under this repo I will publish the ansible playbooks and roles written by myself.

## WORKARROUND_REPOSITORIOS ROLE
This role checks the Oracle Linux public repo configuration when it is corrupted or does not work.

## DNSMASQ_INSTALACION
This playbook installs and configures the dns caching service 'dnsmasq'